package com.pattern.abstractfactory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class ActionMovieFactory implements IMovieFactory {
    @Override
    public ITollywoodMovie getTollywoodMovie() {
        return new TollywoodActionMovie();
    }

    @Override
    public IBollywoodMovie getBollywoodMovie() {
        return new BollywoodActionMovie();
    }
}
