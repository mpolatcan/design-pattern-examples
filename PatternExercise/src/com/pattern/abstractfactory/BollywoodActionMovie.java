package com.pattern.abstractfactory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class BollywoodActionMovie implements IBollywoodMovie {
    @Override
    public String movieName() {
        return "Bang Bang is a Bollywood Action Movie";
    }
}
