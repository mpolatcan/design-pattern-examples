package com.pattern.abstractfactory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class BollywoodComedyMovie implements IBollywoodMovie {
    @Override
    public String movieName() {
        return "Munna Bhai MBBS is a Bollywood Comedy Movie";
    }
}
