package com.pattern.abstractfactory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public interface IMovieFactory {
    ITollywoodMovie getTollywoodMovie();
    IBollywoodMovie getBollywoodMovie();
}
