package com.pattern.abstractfactory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public interface ITollywoodMovie {
    String movieName();
}
