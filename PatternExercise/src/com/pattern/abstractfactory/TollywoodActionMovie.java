package com.pattern.abstractfactory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class TollywoodActionMovie implements ITollywoodMovie {
    @Override
    public String movieName() {
        return "Kranti is a Tollywood Action Movie";
    }
}
