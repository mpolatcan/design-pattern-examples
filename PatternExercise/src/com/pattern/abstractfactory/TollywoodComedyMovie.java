package com.pattern.abstractfactory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class TollywoodComedyMovie implements ITollywoodMovie {
    @Override
    public String movieName() {
        return "BasantaBilap is a Tollywood Comedy Movie";
    }
}
