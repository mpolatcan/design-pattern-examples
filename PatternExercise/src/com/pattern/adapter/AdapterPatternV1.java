package com.pattern.adapter;

/**
 * Created by mpolatcan-gyte_cse on 01.01.2017.
 */
public class AdapterPatternV1 {
    public static void main(String[] args) {
        System.out.println("***** Adapter Pattern Demo *****");
        CalculatorAdapter calculatorAdapter = new CalculatorAdapter();
        Triangle t = new Triangle(20,10);
        System.out.println("Area of triangle: " + calculatorAdapter.getArea(t));
    }
}
