package com.pattern.adapter;

/**
 * Created by mpolatcan-gyte_cse on 01.01.2017.
 */
public class Triangle {
    public double b; // base
    public double h; // height

    public Triangle(int b, int h) {
        this.b = b;
        this.h = h;
    }
}
