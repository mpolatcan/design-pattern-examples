package com.pattern.command;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class CommandPatternV1 {
    public static void main(String[] args) {
        System.out.println("***Command Pattern Demo***\n");
        Receiver intendedReceiver = new Receiver();
        /* Client hold Invoker and Command objects */
        Invoke invoker = new Invoke();
        MyUndoCommand undoCmd = new MyUndoCommand(intendedReceiver);
        MyRedoCommand redoCmd = new MyRedoCommand(intendedReceiver);
        invoker.executeCommand(undoCmd);
        invoker.executeCommand(redoCmd);
    }
}
