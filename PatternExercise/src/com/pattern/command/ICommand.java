package com.pattern.command;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public interface ICommand {
    void Do();
}
