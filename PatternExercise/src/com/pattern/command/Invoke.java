package com.pattern.command;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class Invoke {
    ICommand cmd;

    public void executeCommand(ICommand cmd) {
        this.cmd = cmd;;
        cmd.Do();
    }
}
