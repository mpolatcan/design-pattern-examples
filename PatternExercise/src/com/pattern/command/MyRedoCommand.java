package com.pattern.command;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class MyRedoCommand implements ICommand {
    private Receiver receiver;

    MyRedoCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void Do() {
        // Call redo in receiver
        receiver.performRedo();
    }
}
