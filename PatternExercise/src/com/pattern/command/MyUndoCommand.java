package com.pattern.command;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class MyUndoCommand implements ICommand {
    private Receiver receiver;

    MyUndoCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void Do() {
        // Call undo in receiver
        receiver.performUndo();
    }
}
