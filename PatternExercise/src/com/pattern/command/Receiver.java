package com.pattern.command;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class Receiver {
    public void performUndo() {
        System.out.println("Executing - MyUndoCommand");
    }

    public void performRedo() {
        System.out.println("Executing - MyRedoCommand");
    }
}
