package com.pattern.composite;

/**
 * Created by mpolatcan-gyte_cse on 24.11.2016.
 */
public interface ITeacher {
    String getDetails();
}
