package com.pattern.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mpolatcan-gyte_cse on 24.11.2016.
 */
public class Teacher implements ITeacher {
    private String teacherName;
    private String deptName;
    private List<ITeacher> controls;


    public Teacher(String teacherName, String deptName) {
        this.teacherName = teacherName;
        this.deptName = deptName;
        controls = new ArrayList<ITeacher>();
    }

    @Override
    public String getDetails() {
        return (teacherName + " is the " + deptName);
    }

    public void add(Teacher teacher) {
        controls.add(teacher);
    }

    public void remove(Teacher teacher) {
        controls.remove(teacher);
    }

    public List<ITeacher> getControllingDepts() {
        return controls;
    }

    public void printControllingDepts() {
        for (ITeacher teacher : controls) {
            System.out.println(teacher.getDetails());
            for (ITeacher other : ((Teacher) teacher).getControllingDepts()) {
                System.out.println(other.getDetails());
            }
        }
    }


}
