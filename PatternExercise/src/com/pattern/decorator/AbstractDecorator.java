package com.pattern.decorator;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class AbstractDecorator extends Component {
    protected Component component;

    public void setComponent(Component c) {
        component = c;
    }

    @Override
    public void doJob() {
        if (component != null) {
            component.doJob();
        }
    }
}
