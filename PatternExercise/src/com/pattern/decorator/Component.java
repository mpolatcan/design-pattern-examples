package com.pattern.decorator;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public abstract class Component {
    public abstract void doJob();
}
