package com.pattern.decorator;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class ConcreteComponent extends Component {
    @Override
    public void doJob() {
        System.out.println("I am from Concrete Component - I am closed for " +
                "modification");
    }
}
