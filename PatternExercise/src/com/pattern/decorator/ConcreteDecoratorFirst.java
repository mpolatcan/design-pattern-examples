package com.pattern.decorator;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class ConcreteDecoratorFirst extends AbstractDecorator {
    @Override
    public void doJob() {
        super.doJob();

        // Add additional thing if necessary
        System.out.println("I am explicitly from DecoratorFirst");
    }
}
