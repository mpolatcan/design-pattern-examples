package com.pattern.decorator;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class ConcreteDecoratorSecond extends AbstractDecorator {
    @Override
    public void doJob() {
        System.out.println("");
        System.out.println("***START Ex-2***");
        super.doJob();
        // Add additional thing if necessary
        System.out.println("Explicitly from DecoratorSecond");
    }
}
