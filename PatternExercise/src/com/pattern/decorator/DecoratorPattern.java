package com.pattern.decorator;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class DecoratorPattern {
    public static void main(String[] args) {
        System.out.println("Decorator Pattern Demo***");
        ConcreteComponent cc = new ConcreteComponent();

        ConcreteDecoratorFirst cdFirst = new ConcreteDecoratorFirst();
        // Decorating ConcreteComponent Object // cc with ConcreteDecoratorFirst
        cdFirst.setComponent(cc);
        cdFirst.doJob();

        ConcreteDecoratorSecond cdSecond = new ConcreteDecoratorSecond();
        // Decorating ConcreteComponent Object // cc with ConcreteDecoratorSecondc
        cdSecond.setComponent(cc); // Adding result from cdFirst now
        cdSecond.doJob();
    }
}
