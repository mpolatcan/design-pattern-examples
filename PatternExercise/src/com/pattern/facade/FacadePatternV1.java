package com.pattern.facade;

/**
 * Created by mpolatcan-gyte_cse on 02.01.2017.
 */
public class FacadePatternV1 {
    public static void main(String[] args) {
        System.out.println("*** Facade Pattern Demo ***");
        RobotFacade rf1 = new RobotFacade();
        rf1.constructRobot("Green","Iron");
        RobotFacade rf2 = new RobotFacade();
        rf2.constructRobot("Blue","Steel");
    }
}
