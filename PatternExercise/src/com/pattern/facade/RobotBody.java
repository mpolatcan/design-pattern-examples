package com.pattern.facade;

/**
 * Created by mpolatcan-gyte_cse on 01.01.2017.
 */
public class RobotBody {
    public void createBody() {
        System.out.println("Body creation done");
    }
}
