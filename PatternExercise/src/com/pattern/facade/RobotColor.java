package com.pattern.facade;

/**
 * Created by mpolatcan-gyte_cse on 01.01.2017.
 */
public class RobotColor {
    private String color;
    public void setColor(String color) {
        this.color = color;
        System.out.println("Color is set to: " + this.color);
    }
}
