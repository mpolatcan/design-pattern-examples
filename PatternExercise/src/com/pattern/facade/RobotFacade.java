package com.pattern.facade;

/**
 * Created by mpolatcan-gyte_cse on 01.01.2017.
 */
/*
It is one of those patterns that supports loose coupling. Here we emphasize the
abstraction and hide the complex details by exposing a simple interface.
 */
public class RobotFacade {
    RobotColor rc;
    RobotMetal rm;
    RobotBody rb;

    public RobotFacade() {
        rc = new RobotColor();
        rm = new RobotMetal();
        rb = new RobotBody();
    }

    public void constructRobot(String color, String metal) {
        System.out.println("\nCreation of the Robot Start");
        rc.setColor(color);
        rm.setMetal(metal);
        rb.createBody();
        System.out.println("\nRobot creation end");
        System.out.println();
    }
}
