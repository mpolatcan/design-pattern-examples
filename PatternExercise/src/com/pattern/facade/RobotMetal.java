package com.pattern.facade;

/**
 * Created by mpolatcan-gyte_cse on 01.01.2017.
 */
public class RobotMetal {
    private String metal;
    public void setMetal(String metal) {
        this.metal = metal;
        System.out.println("Metal is set to : " + this.metal);
    }
}
