package com.pattern.factory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class ConcreteFactory extends IAnimalFactory {
    @Override
    public IAnimal getAnimalType(String type) throws Exception {
        if (type.equals("Duck"))
            return new Duck();
        else if (type.equals("Tiger"))
            return new Tiger();
        else
            throw new Exception("Animal type: " + type + " cannot be" +
                    " instantiated");
    }
}
