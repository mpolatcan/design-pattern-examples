package com.pattern.factory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class Duck implements IAnimal {
    @Override
    public void speak() {
        System.out.println("Duck says Pack-pack");
    }
}
