package com.pattern.factory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class FactoryPatternV1 {
    public static void main(String[] args) {
        System.out.println("***Factory Pattern Demo***\n");
        IAnimalFactory animalFactory = new ConcreteFactory();
        try {
            IAnimal DuckType = animalFactory.getAnimalType("Duck");
            DuckType.speak();

            IAnimal TigerType = animalFactory.getAnimalType("Tiger");
            TigerType.speak();

            // There is no Lion type. So an exception will be thrown
            IAnimal LionType = animalFactory.getAnimalType("Lion");
            LionType.speak();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }
}
