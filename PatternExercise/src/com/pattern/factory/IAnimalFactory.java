package com.pattern.factory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public abstract class IAnimalFactory {
    // If we cannot instantiate in later stage, we'll throw exception */
    public abstract IAnimal getAnimalType(String type) throws Exception;
}
