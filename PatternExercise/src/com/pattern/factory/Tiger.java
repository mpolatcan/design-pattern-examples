package com.pattern.factory;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class Tiger implements IAnimal {
    @Override
    public void speak() {
        System.out.println("Tiger says: Halum...Halum");
    }
}
