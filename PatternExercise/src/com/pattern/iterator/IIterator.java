package com.pattern.iterator;

/**
 * Created by mpolatcan-gyte_cse on 02.01.2017.
 */
public interface IIterator {
    void first(); // reset to first element
    String next(); // get next element
    Boolean isDone(); // end of collection
    String currentItem(); // retrieve current item
}
