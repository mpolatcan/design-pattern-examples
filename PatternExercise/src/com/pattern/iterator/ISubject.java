package com.pattern.iterator;

/**
 * Created by mpolatcan-gyte_cse on 02.01.2017.
 */
public interface ISubject {
    public IIterator createIterator();
}
