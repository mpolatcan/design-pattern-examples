package com.pattern.iterator;

/**
 * Created by mpolatcan-gyte_cse on 02.01.2017.
 */
public class IteratorPatternV1 {
    public static void main(String[] args) {
        System.out.println("***** Iterator Pattern Demo *****");
        ISubject Sc_subjects = new Science();
        ISubject Ar_subjects = new Arts();
        IIterator Sc_iterator = Sc_subjects.createIterator();
        IIterator Ar_iterator = Ar_subjects.createIterator();

        System.out.println("\nScience subjects: ");
        print(Sc_iterator);

        System.out.println("\nArts subjects: ");
        print(Ar_iterator);
    }

    public static void print(IIterator iterator) {
        while (!iterator.isDone()) {
            System.out.println(iterator.next());
        }
    }
}
