import java.util.Iterator;

/**
 * Created by mpolatcan-gyte_cse on 04.01.2017.
 */
public class SpiralIteratorTest {
    public static String[] strArray = new String[5];

    public static void main(String[] args) {
        Integer[][] array = new Integer[4][4];
        array[0][0] = 1;
        array[0][1] = 2;
        array[1][0] = 5;
        array[1][1] = 6;
        array[2][0] = 9;
        array[2][1] = 10;
        array[3][0] = 13;
        array[3][1] = 14;

        /*
        * 1,2
        * 5,6
        * 9,10
        * 13,14
        * */
        SpiralIterator iterator = new SpiralIterator(array,2,4);

        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }

    }

    public static class SpiralIterator implements Iterator {
        private Integer[][] array;
        private boolean[][] visited;
        private int posX, posY, col, row, direction = 1;
        private boolean hasMoreElements = true;

        public SpiralIterator(Integer[][] array, int col, int row) {
            this.array = array;
            posX = 0;
            posY = 0;
            this.col = col;
            this.row = row;
            visited = new boolean[row][col];
        }

        @Override
        public boolean hasNext() {
            return hasMoreElements;
        }

        @Override
        public Object next() {
            Object nextObject = array[posY][posX];
            visited[posY][posX] = true;

            if (!allElementsVisited()) {
                if (direction == 1) {
                    if (posX + 1 < col && !visited[posY][posX+1]) {
                        ++posX;
                    } else {
                        direction = 2;
                        next();
                    }
                } else if (direction == 2) {
                    if (posY + 1 < row && !visited[posY+1][posX]) {
                        ++posY;
                    } else {
                        direction = 3;
                        next();
                    }
                } else if (direction == 3) {
                    if (posX - 1 >= 0 && !visited[posY][posX-1]) {
                        --posX;
                    } else {
                        direction = 4;
                        next();
                    }
                } else if (direction == 4) {
                    if (posY - 1 >= 0 && !visited[posY-1][posX]) {
                        --posY;
                    } else {
                        direction = 1;
                        next();
                    }
                }
            } else {
                hasMoreElements = false;
            }

            return nextObject;
        }

        public boolean allElementsVisited() {
            boolean allElementsVisited = true;

            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    if (!visited[i][j])
                        allElementsVisited = false;
                }
            }

            return allElementsVisited;
        }
    }
}
