package com.pattern.observer;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public interface ISubject {
    void register(Observer o);
    void unregister(Observer o);
    void notifyObservers();
}
