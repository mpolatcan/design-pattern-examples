package com.pattern.observer.v2;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class ObserverFirst implements IObserver{
    @Override
    public void update(int i) {
        System.out.println("ObserverFirst: myValue in Subject is now: " + i);
    }
}
