package com.pattern.observer.v2;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class ObserverSecond implements IObserver {
    @Override
    public void update(int i) {
        System.out.println("ObserverSecond: observes -> myValue is changed in" +
                " Subject to :" + i);
    }
}
