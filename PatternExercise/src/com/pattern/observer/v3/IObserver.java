package com.pattern.observer.v3;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public interface IObserver {
    void update(String s, int i);
}
