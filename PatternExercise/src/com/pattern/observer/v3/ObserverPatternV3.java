package com.pattern.observer.v3;


/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class ObserverPatternV3 {
    public static void main(String[] args) {
        System.out.println("***Observer Pattern Demo 3***\n");
        SubjectFirst sub1 = new SubjectFirst();
        SubjectSecond sub2 = new SubjectSecond();

        ObserverFirst ob1 = new ObserverFirst();
        ObserverSecond ob2 = new ObserverSecond();
        ObserverThird ob3 = new ObserverThird();

        // ObserverFirst and ObserverSecond registers to SubjectFirst
        sub1.register(ob1);
        sub1.register(ob2);

        //ObserverSecond and ObserverThird registers to SubjectSecond
        sub2.register(ob2);
        sub2.register(ob3);

        // Set new value to SubjectFirst
        // ObserverFirst and ObserverSecond get notification
        sub1.setMyValue(50);
        System.out.println();

        // Set new value to SubjectSecond
        // ObserverSecond and ObserverThird get notification
        sub2.setMyValue(250);
        System.out.println();

        // unregister ObserverSecond from SubjectFirst
        sub1.unregister(ob2);

        // Set new value to Subject & only ObserverFirst notified
        sub1.setMyValue(550);
        System.out.println();

        // ob2 can still notice change in SubjectSecond
        sub2.setMyValue(750);
    }
}
