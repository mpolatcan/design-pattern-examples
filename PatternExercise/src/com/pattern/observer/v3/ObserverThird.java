package com.pattern.observer.v3;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class ObserverThird implements IObserver {
    @Override
    public void update(String s, int i) {
        System.out.println("ObserverThird is observing: myValue is " +
                " changed " + s + "to: " + i);
    }
}
