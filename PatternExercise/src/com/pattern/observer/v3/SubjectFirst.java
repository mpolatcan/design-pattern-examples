package com.pattern.observer.v3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class SubjectFirst implements ISubject {
    private int myValue;
    private List<IObserver> observerList = new ArrayList<IObserver>();

    public int getMyValue() {
        return myValue;
    }

    public void setMyValue(int myValue) {
        this.myValue = myValue;

        // Notify observers
        notifyObservers(myValue);
    }

    @Override
    public void register(IObserver o) {
        observerList.add(o);
    }

    @Override
    public void unregister(IObserver o) {
        observerList.remove(o);
    }

    @Override
    public void notifyObservers(int updatedValue) {
        for (int i = 0; i < observerList.size(); i++) {
            observerList.get(i).update(this.getClass().getSimpleName(),
                    updatedValue);
        }
    }
}
