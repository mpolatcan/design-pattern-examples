package com.pattern.proxy;

/**
 * Created by mpolatcan-gyte_cse on 02.01.2017.
 */
public class ConcreteSubject extends Subject {
    @Override
    public void doSomework() {
        System.out.println("I am from concrete subject");
    }
}
