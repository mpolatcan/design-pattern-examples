package com.pattern.proxy;

/**
 * Created by mpolatcan-gyte_cse on 02.01.2017.
 */
public class Proxy extends Subject {
    private ConcreteSubject cs;

    @Override
    public void doSomework() {
        System.out.println("\nProxy call happening now");
        if (cs == null) {
            cs = new ConcreteSubject();
        }
        cs.doSomework();
    }
}
