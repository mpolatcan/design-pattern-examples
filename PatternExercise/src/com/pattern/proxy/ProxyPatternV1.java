package com.pattern.proxy;

/**
 * Created by mpolatcan-gyte_cse on 02.01.2017.
 */
public class ProxyPatternV1 {
    public static void main(String[] args) {
        System.out.println("***** Proxy Pattern Demo *****");
        Proxy px = new Proxy();
        px.doSomework();
    }
}
