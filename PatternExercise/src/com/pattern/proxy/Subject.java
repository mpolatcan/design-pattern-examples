package com.pattern.proxy;

/**
 * Created by mpolatcan-gyte_cse on 02.01.2017.
 */
public abstract class Subject {
    public abstract void doSomework();
}
