package com.pattern.singleton;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
/*
This method does not need to use the synchronization technique and eager initialization.
It is regarded as the standard method to implement singletons in Java.
 */
public class MakeACaptainV2 {
    private static MakeACaptainV2 _captain;
    private MakeACaptainV2() {

    }

    // Bill Pugh solution
    private static class SingletonHelper {
        // Nested class is referenced after getCaptain is called
        private static final MakeACaptainV2 _captain = new MakeACaptainV2();
    }

    public static MakeACaptainV2 getCaptain() {
        return SingletonHelper._captain;
    }
}
