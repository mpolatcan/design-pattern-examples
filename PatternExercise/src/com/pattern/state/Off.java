package com.pattern.state;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class Off extends RemoteControl {
    @Override
    public void pressSwitch(TV context) {
        System.out.println("I am Off. Going to be On now");
        context.setState(new On());
    }
}
