package com.pattern.state;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class On extends RemoteControl {
    @Override
    public void pressSwitch(TV context) {
        System.out.println("I am already On. Going to be Off now");
        context.setState(new Off());
    }
}
