package com.pattern.state;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public abstract class RemoteControl {
    public abstract void pressSwitch(TV context);
}
