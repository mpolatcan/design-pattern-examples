package com.pattern.state;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class StatePatternV1 {
    public static void main(String[] args) {
        System.out.println("*******State Pattern Demo v1*******\n");
        Off initialState = new Off();
        TV tv = new TV(initialState);
        tv.pressButton();
        tv.pressButton();
    }
}
