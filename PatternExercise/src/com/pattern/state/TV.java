package com.pattern.state;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class TV {
    private RemoteControl state;

    public RemoteControl getState() {
        return state;
    }

    public void setState(RemoteControl state) {
        this.state = state;
    }

    public TV(RemoteControl state) {
        this.state = state;
    }

    public void pressButton() {
        state.pressSwitch(this);
    }
}
