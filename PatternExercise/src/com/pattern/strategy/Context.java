package com.pattern.strategy;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class Context {
    private IChoice myChoice;

    public void setMyChoice(IChoice myChoice) {
        this.myChoice = myChoice;
    }

    public void showChoice(String s1, String s2) {
        myChoice.myChoice(s1,s2);
    }
}
