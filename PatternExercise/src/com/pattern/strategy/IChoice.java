package com.pattern.strategy;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public interface IChoice {
    void myChoice(String s1, String s2);
}
