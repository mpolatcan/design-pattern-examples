package com.pattern.strategy;

import java.util.Scanner;

/**
 * Created by mpolatcan-gyte_cse on 15.11.2016.
 */
public class StrategyPatternV1 {
    public static void main(String[] args) {
        System.out.println("***Strategy Pattern Demo***");
        Scanner in = new Scanner(System.in);
        IChoice choice = null;
        Context ctx = new Context();
        String input1, input2;

        try {
            for (int i = 0; i <= 2; i++) {
                System.out.println("Enter an integer: ");
                input1 = in.nextLine();
                System.out.println("Enter another integer: ");
                input2 = in.nextLine();
                System.out.println("Enter ur choice (1 or 2)");
                System.out.println("Press 1 for Addition, 2 for Concatenation");
                String c = in.nextLine();

                if (c.equals("1")) {
                    choice = new FirstChoice();
                } else {
                    choice = new SecondChoice();
                }

                ctx.setMyChoice(choice);
                ctx.showChoice(input1,input2);
            }
        } finally {
            in.close();
        }
        System.out.println("End of strategy pattern");
    }
}
