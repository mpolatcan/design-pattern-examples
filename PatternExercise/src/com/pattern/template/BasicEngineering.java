package com.pattern.template;

/**
 * Created by mpolatcan-gyte_cse on 27.11.2016.
 */
public abstract class BasicEngineering {
    public void Papers() {
        // Common Papers:
        Math();
        SoftSkills();
        // Specialized Paper:
        SpecialPaper();
    }

    // Non-abstract method Math(), SoftSkills(), are
    // already implemented by Template class
    private void Math() {
        System.out.println("Mathematics");
    }

    private void SoftSkills() {
        System.out.println("SoftSkills");
    }

    // abstract method SpecialPaper() must be implemented in derived classes
    public abstract void SpecialPaper();
}
