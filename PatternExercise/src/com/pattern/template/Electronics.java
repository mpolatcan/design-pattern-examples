package com.pattern.template;

/**
 * Created by mpolatcan-gyte_cse on 27.11.2016.
 */
public class Electronics extends BasicEngineering {
    @Override
    public void SpecialPaper() {
        System.out.println("Digital Logic and Circuit Theory");
    }
}
