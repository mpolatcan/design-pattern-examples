package com.pattern.template;

/**
 * Created by mpolatcan-gyte_cse on 27.11.2016.
 */
public class TemplateMethodPatternV1 {
    public static void main(String[] args) {
        System.out.println("*** Template Method Pattern Demo ***\n");
        BasicEngineering bs = new ComputerScience();
        System.out.println("Computer Science Papers: ");
        bs.Papers();
        System.out.println();
        bs = new Electronics();
        System.out.println("Electronic Papers: ");
        bs.Papers();
    }
}
